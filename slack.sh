#!/bin/sh

PROJECT_NAME=$1
JOB_ID=$2
PAGES_URL=$3

newUrl="$PAGES_URL/-/$PROJECT_NAME/-/jobs/$JOB_ID/artifacts/public/site/allure-maven-plugin/index.html"

message="Olá Pessoal, @aqui 
Projeto:  $PROJECT_NAME
Link Resultado: $newUrl"

curl --location --request POST 'https://hooks.slack.com/services/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' \
--header 'Content-Type: application/json' \
--data-raw "{ \"text\": \"$message\", \"channel\": \"C069NKYGEN7\"}"
