package com.api.variaveis;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class variaveis {

    public static Response response;
    public static RequestSpecification request;
    public static int maximoTentativas = 3;

}
