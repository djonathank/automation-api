package com.api.actions;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import lombok.extern.log4j.Log4j2;
import org.apache.groovy.json.internal.LazyMap;
import org.junit.jupiter.api.Assertions;

import java.util.Map;

import static com.api.variaveis.variaveis.maximoTentativas;
import static com.api.variaveis.variaveis.response;
import static io.restassured.RestAssured.given;

@Log4j2
public class acoesApi {

    public static void setBaseURI(String baseURI) {
        RestAssured.baseURI = baseURI;
    }

    public static void setBasePath(String endpoint) {
        RestAssured.basePath = endpoint;
    }

    /**
     * Chamada Get
     * Header: Cabecalho contendo campo e valor
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executeGet(LazyMap header, ContentType contentType) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .log().all()
                            .when()
                            .get()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }

    /**
     * Chamada Get
     * Header: Cabecalho contendo campo e valor
     * Param: são os parâmetros necessário para a requisição
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executeGet(LazyMap header, LazyMap param, ContentType contentType) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .params(param)
                            .log().all()
                            .when()
                            .get()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }

    /**
     * Chamada POST
     * Header: Cabecalho contendo campo e valor
     * Json: pode ser uma String, Map ou uma classe Chamada sendo a informacao necessaria para requisicao
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executePost(LazyMap header, Object json, ContentType contentType) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .body(json)
                            .log().all()
                            .when()
                            .post()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }

    /**
     * Chamada POST
     * Header: Cabecalho contendo campo e valor
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executePost(LazyMap header, ContentType contentType) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .log().all()
                            .when()
                            .post()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }

    /**
     * Chamada POST
     * Header: Cabecalho contendo campo e valor
     * params: Lista de map de params
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executePostFormParam(LazyMap header, ContentType contentType, Map<String, String> params) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .formParams(params)
                            .log().all()
                            .when()
                            .post()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }

    /**
     * Chamada DELETE
     * Header: Cabecalho contendo campo e valor
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executeDelete(LazyMap header, ContentType contentType) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .log().all()
                            .when()
                            .delete()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }

    /**
     * Chamada DELETE
     * Header: Cabecalho contendo campo e valor
     * Json: pode ser uma String, Map ou uma classe sendo a informacao necessaria para requisicao
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executePut(LazyMap header, Object json, ContentType contentType) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .body(json)
                            .log().all()
                            .when()
                            .put()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }

    /**
     * Chamada PUT
     * header: Cabecalho contendo campo e valor
     * params: Lista de map de params
     * ContentYype: Enum que pode JSON, XML, UrlEnconded entre outros
     */
    public static void executePutParams(LazyMap header, LazyMap params, ContentType contentType) {
        for (int i = 0; i < maximoTentativas; i++) {
            response =
                    given()
                            .relaxedHTTPSValidation()
                            .contentType(contentType)
                            .headers(header)
                            .params(params)
                            .log().all()
                            .when()
                            .put()
                            .then()
                            .log().all()
                            .extract().response();
            if (response.statusCode() < 500) {
                return;
            }
        }
        Assertions.fail("Erro no Status Code: " + response.statusCode());
    }
}
