package com.api.actions;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import lombok.extern.log4j.Log4j2;
import org.apache.groovy.json.internal.LazyMap;
import org.junit.jupiter.api.Assertions;

import java.time.Duration;
import java.util.Map;

import static com.api.variaveis.variaveis.maximoTentativas;
import static com.api.variaveis.variaveis.response;
import static io.restassured.RestAssured.given;

@Log4j2
public class acoesComuns {

    /* Esperar Duração de Segundos */
    public static Duration esperaDuracaoSegundos(int tempo) {
        return Duration.ofSeconds(tempo);
    }
}
