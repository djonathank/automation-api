package com.api.runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        tags = "@api",
        glue = {"com/api"},
        plugin = {
                "pretty", "json:target/cucumber/cucumber-reports.json",
                "pretty", "html:target/cucumber/cucumber-reports.html",
                "junit:target/xml-junit/junit.xml",
                "io.qameta.allure.cucumber7jvm.AllureCucumber7Jvm"
        },
        features = {"src/test/resources/com/api/features"})
public class TestRunner {
}