package com.api.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.api.utils.Utils.*;

@Log4j2
public class Hooks {

    public static String caminho_evidencia_erro;
    public static String testName;

    @Before("@api")
    public void before(Scenario cenario) {
        testName = cenario.getName();
    }

    @After("@api")
    public void after(Scenario cenario) {

    }
}
